
from flask import Flask,flash,render_template
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_compress import Compress
from colored_print import ColoredPrint


# Information logging on terminal

logger = ColoredPrint()



db = SQLAlchemy()
login_manager = LoginManager()
login_manager.session_protection = 'strong'
login_manager.login_view = 'main.login'

# Project globals

DB_NAME = "userinfo.db"
DEBUG = True
CACHE_TIMEOUT = 3600

ENABLE_REGISTER = False
CREATE_DB = True

# Get the absolute path from given path relative to the current file
def abspath(currpath):
    from os import path
    HOME = path.abspath(path.dirname(f"{__name__}"))
    return path.abspath(path.join(HOME,currpath))


flash_messsages =  {
    404:"The requested resource not found",
    500:"Internal server error",
}


def create_app():
    app = Flask(__name__)

    # Configuring the application

    if DEBUG:
        app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
    else:
        app.config['SEND_FILE_MAX_AGE_DEFAULT'] = CACHE_TIMEOUT

    app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///database/{DB_NAME}'
    app.config['SECRET_KEY'] = 'CHANGE THIS KEY'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    app.config['TEMPLATES_AUTO_RELOAD'] = True
    app.jinja_env.add_extension('jinja2.ext.do')

    # Initializing the flask extensions
    Compress(app)
    db.init_app(app)
    login_manager.init_app(app)
    login_manager.session_protection = "strong"


    from .views import main as main_blueprint
    app.register_blueprint(main_blueprint)

    @app.errorhandler(404)
    @app.errorhandler(500)
    def pageNotFound(error):
        flash(flash_messsages[error.code])
        return render_template("error.html",error=error),error.code

    return app