from flask import Blueprint,render_template,request, jsonify, redirect, url_for, g,flash,abort,send_file,send_from_directory, make_response
from flask_login import login_user,login_required, logout_user, current_user
from .models import  User
from .forms import RegisterForm,LoginForm
from . import ENABLE_REGISTER, db,logger,abspath
import json
import os

main = Blueprint('main',__name__)

dirname = abspath("..")

@main.route('/')
@login_required
def index():
    return render_template('index.html',artist=getArtistList())

@main.route('/login', methods=["GET", "POST"])
def login():
    form1 = LoginForm(request.form)
    if form1.validate_on_submit():
        user = User.query.filter_by(email=form1.email.data).first()
        if user is not None and user.verify_password(form1.password.data):
            login_user(user,remember=form1.remember_me)
            logger.info(form1.remember_me.data)
            return redirect(url_for("main.index"))

    return render_template('login.html', form1=form1)


@main.route('/register', methods=["GET", "POST"])
def register():
    if ENABLE_REGISTER:
        form2 = RegisterForm(request.form, meta={'csrf':False})
        if form2.validate_on_submit():
            new_user = User(email=form2.email.data,
                    username=form2.username.data,
                    password=form2.password.data)
            db.session.add(new_user)
            db.session.commit()
            flash("Registered successfully","success")
            return redirect(url_for('main.login'))
        return render_template('register.html',form2=form2)
    else:
        flash("Registeration disabled, please contact admin at dhairya.cse@gmail.com","error")
        return redirect(url_for("main.login"))

@main.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('main.index'))


@main.route("/<artist>/songs")
def songListDispatcher(artist):
    return send_file(os.path.join(dirname,"Data/SongDB/%s/songList.json"%artist))

@main.route("/songDB")
def getSongDB():
    return send_file(os.path.join(dirname,"Data/config.json"))

@main.route("/error")
def errorFromUser():
    message = request.args.get('message')
    if message:
        flash(message,"error")
    abort(500)

def getArtistList():
    with open(os.path.join(dirname,"Data/artistList.json")) as fp:
        return json.load(fp)

def getSongList(artist):
    with open(os.path.join(dirname,"Data/SongDB/%s/songList.json"%artist)) as fp:
        return json.load(fp)

@main.route("/download/<artistName>/<songName>/<fileName>")
def downloadSongData(artistName,songName,fileName):
    response = make_response(send_from_directory(dirname,f"Data/SongDB/{artistName}/{songName}/{fileName}",conditional=True,attachment_filename=fileName))
    if str.endswith(fileName,"gz"):
        response.headers['Content-Encoding']='gzip'
    return response

@main.route("/wasm/<path:filename>")
def serveWASM(filename):
    return send_from_directory(dirname,f"wasm/{filename}")

@main.route('/service_worker.js')
def serviceWorker():
    return send_file(abspath('app/static/service_worker.js')), 200, {'Content-Type': 'text/javascript'}

@main.route('/manifest')
def mainfest():
    return send_file(abspath('app/static/site.webmanifest'))

@main.route('/offline')
def offline():
    return render_template("offline.html")

@main.route('/upload/stats',methods=['POST'])            
def uploadStats():
    res = request.json
    res["addr"]=request.remote_addr
    with open(abspath("../site_stats"),"a") as f:
        f.write(json.dumps(res))
        f.write("\n")
    return ""

@main.route('/help')
def help():
    return render_template("help.html")

@main.context_processor
def utility_processor():    
    return dict(getSongList=getSongList)