# Whistler

This is a mini-project developed to help me in my major project for my masters degree.

The objective of this mini-project is to record an audio using client's browser, analyze it's pitch and tonic using essentia.js and then synthesize and audio from it.

**Tool stack:**
- **Frontend** :HTML, JS, W3-CSS
- **Backend** : flask, python, Jinja2
