
from os import path,makedirs
import sys

from app import *
    


def create_db():
    app = create_app()
    with app.app_context():
        db.create_all()

def main():
    if CREATE_DB or not path.exists(abspath(f'app/database/{DB_NAME}')):
        makedirs(abspath('app/database/'),exist_ok=True)
        logger.success("[Database] Created a new database")
        create_db()
    else:
        logger.info("[Database] Using previously created database")
    app = create_app()
    ssl_context=(abspath('certificate/cert.pem'), abspath('certificate/key.pem'))
    app.run(host="0.0.0.0",debug=DEBUG,ssl_context=ssl_context)

if __name__=='__main__':
    main()