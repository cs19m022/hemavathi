from app import *

webapp = create_app()

if CREATE_DB:
    with webapp.app_context():
            db.create_all()